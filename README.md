
### What is this repository for? ###

* This is a small POC application which is built on React+NodeJS+MongoDB, which has 3 layers: Database, Backend/API and FrontEnd.
* Frontend has two parts, one for Admin and one for External users.
* Admin Frontend shows a table of all external users and their info (this page is authentication protected).
* External users Frontend provide a simple form for users to enter their personal info including First Name, Last Name, Telephone Number, Full Address, and SSN. In the Backend, we are receiving the user's info and do the following:
○ Encrypt SSN and store it in the DB
○ Store the rest of info as plain text in the DB

### How do I get set up? ###

To run the NODE api

* `cd api`
* `npm install`
* `npm start`

To run the NODE api

* `cd user-app`
* `npm install`
* `npm start`
