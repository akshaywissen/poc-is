
const initalState = {
    currentUser: {},
    isLoggedIn: false
}

export function authreducer(state = initalState, action) {
    let { type } = action;
    switch (type) {
        case "LOGIN_SUCCESS": {
            const { user } = action;
            return { ...state, currentUser: user, isLoggedIn: true };
        }
        
        case "LOGOUT_SUCCESS": {
            const { user } = action;
            return { ...state, currentUser: null, isLoggedIn: false };
        }
        default:
            return state;
    }
}