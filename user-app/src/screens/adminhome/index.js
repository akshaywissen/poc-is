import React, { Component } from 'react'
import { connect } from 'react-redux'
import { loadUsers } from '../../actions/users';
import { withRouter } from 'react-router-dom';
import {
  Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper,
  Icon, Typography
} from '@material-ui/core';
import './styles.css';
class AdminHome extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { loadUsers, isLoggedIn } = this.props;
    if(!isLoggedIn) {
      this.props.history.push("/");
    }
    loadUsers();
  }

  render() {
    const { userList } = this.props;
    if (userList.length === 0)
      return (
        <div className="emptylist">
          <Typography variant="h5" component="h3">
          No users added yet !!!
          </Typography>
         </div>
      )
    else
      return (
        <div className="adminhome-container">
          <TableContainer component={Paper}>
            <Table aria-label="user table">
              <TableHead>
                <TableRow>
                  <TableCell>#</TableCell>
                  <TableCell align="center">Name</TableCell>
                  <TableCell align="center">Telephone Number</TableCell>
                  <TableCell align="center">Address</TableCell>
                  {/* <TableCell align="center">SSN</TableCell> */}
                </TableRow>
              </TableHead>
              <TableBody>
                {userList.map((user, index) => (
                  <TableRow key={user._id}>
                    <TableCell>
                      { index + 1 }
                    </TableCell>
                    <TableCell align="center"><div><Icon>account_circle</Icon>{ user.firstName + ' ' + user.lastName }</div></TableCell>
                    <TableCell align="center">{user.telephoneNo}</TableCell>
                    <TableCell align="center">{user.address}</TableCell>
                    {/* <TableCell align="center">{user.ssn}</TableCell> */}
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      )
  }
}


const mapStateToProps = (state) => {
  const { userList = [] } = state.users;
  const { isLoggedIn = false } = state.auth;
  return { userList, isLoggedIn }
}
const mapDispatchToProps = { loadUsers }

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminHome));