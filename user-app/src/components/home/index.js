import { AppBar, Toolbar, Typography, Button } from '@material-ui/core';
import { Link, withRouter } from 'react-router-dom';
import React, { Component } from 'react'
import { connect } from 'react-redux'  
import './index.css';

class Home extends Component {

    componentDidMount() {}

    constructor(props) {
        super(props)
    }

    render() {
        const { isLoggedIn } = this.props;
        return (
            <div className="main">
                <Typography variant="h6">
                    Please login or register yourself, by clicking on add button from header.
                </Typography>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { isLoggedIn = false } = state.auth;
    return { isLoggedIn }
}


export default withRouter(connect(mapStateToProps, null)(Home));


