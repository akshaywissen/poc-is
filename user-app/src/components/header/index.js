import { AppBar, Toolbar, Typography, Button } from '@material-ui/core';
import { Link, withRouter } from 'react-router-dom';
import React, { Component } from 'react'
import { connect } from 'react-redux'  
import './index.css';
import { logout } from "../../actions/auth";

class Header extends Component {

    componentDidMount() {}

    constructor(props) {
        super(props)
    }

    doLogout() {
        const { logout } = this.props;
        logout(() => {
            this.props.history.push('/')
        })
    }
    render() {
        const { isLoggedIn } = this.props;
        return (
            <div>
                <AppBar position="static" className="header-bar">
                    <Toolbar>
                        <Typography variant="h6">
                            React Demo
                        </Typography>
                        <div>
                            <Button className="add-btn" variant="outlined" color="inherit" component={Link} to="/userform">
                                Add
                            </Button>
                            { 
                                isLoggedIn ? 
                                (
                                    <Button variant="outlined" color="inherit" onClick={ this.doLogout.bind(this) }>
                                        Logout
                                    </Button>
                                ) : (
                                    <Button variant="outlined" color="inherit" component={Link} to="/login">
                                        Login
                                    </Button>
                                )
                            }
                        </div>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { isLoggedIn = false } = state.auth;
    return { isLoggedIn }
}

const mapDispatchToProps = { logout };

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));


