/** Copyright (c) 2020 - Present, Wissen Technology**/
const crypto = require('crypto');

const ALGORITHM = 'aes256',
      KEY = '3zTvzr3p67VC61jmV54rIYu1545x4TlY',
      IV = '60iP0h6vJoEaPl9k';

exports.encrypt = (text) => {
  const cipher = crypto.createCipheriv(ALGORITHM, KEY, IV)
  let encrypted = cipher.update(text, 'utf8', 'hex')
  encrypted += cipher.final('hex');
  return encrypted;
}

exports.decrypt = (encrypted) => {
  var decipher = crypto.createDecipheriv(ALGORITHM, KEY, IV)
  var dec = decipher.update(encrypted, 'hex', 'utf8')
  dec += decipher.final('utf8');
  return dec;
}
