var express = require('express');
var router = express.Router();

const loginController = require('../controllers/login/login');
const loginSchema = require('../controllers/login/schema');

const { wrapControllerFunction, validate} = require('../middlewares')

router.post('/signin', validate(loginSchema.signIn), wrapControllerFunction(loginController.signIn.bind(loginController)));

module.exports = router;
