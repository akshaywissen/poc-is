const mongoose = require('mongoose');
const crypto = require('../security/crypto');

const Schema = mongoose.Schema;

const Users = new Schema ({
    firstName: { 
        type: String, 
        required: true 
    },
    lastName: { 
        type: String, 
        required: true 
    },
    telephoneNo: { 
        type: String, 
        required: true 
    },
    address: { 
        type: String, 
        required: true 
    },
    ssn: { 
        type: String, 
        required: true,
        unique: true,
        set(value) {
           return crypto.encrypt(value);
        },
        get(value) {
            return crypto.decrypt(value);
        }
    },
    status: { 
        type: Number, 
        default: 1 
    }
});

Users.set('toJSON', { getters: true });

module.exports = mongoose.model('Users', Users);