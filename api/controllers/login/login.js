'use strict';

const jwt = require('jsonwebtoken');
const acronym = require('../../config/acronyms')
const lang   = require('../../config/lang');
const { JWTKey } = require('../../config/config');
const Login = require('../../models/logins');
const crypto = require('../../security/crypto');

const login = {
    
    /**
     * login login
     * @param {Object} req 
     */
    async signIn(req) {
        try {
            const { username, password } = req.body;
    
            const usernameCondition = { username };
            const loginUser = await Login.findOne(usernameCondition).exec();
            if(loginUser) {
                if(loginUser.password === password) {
                    const { _id: id, username } = loginUser;
                    
                    const authToken = jwt.sign(`${ id }-${ username }`, JWTKey);
                    
                    loginUser._doc.authToken = authToken;
                    delete loginUser._doc.password;

                    return {
                        success: true,
                        responseStatus: acronym.responseStatus.SUCCESS,
                        message: lang.success.userLoggedIn,
                        data: {
                            user: loginUser
                        }
                    }
                }
                return {
                    success: false,
                    responseStatus: acronym.responseStatus.UNAUTHORIZED,
                    message: lang.error.worngPassword,
                }
            } 
            return {
                success: false,
                responseStatus: acronym.responseStatus.UNAUTHORIZED,
                message: lang.error.worngUsername,
            }
            
        } catch(err) {
            global.CONSOLE(err)
            return {
                success: false,
                responseStatus: acronym.responseStatus.INTERNALSERVERERROR,
                errorType: acronym.errorType.SERVERERROR,
                errorMessage: acronym.errorMessage.SERVERERRORMESSAGE,
                errorsArray: []
            }
        }
    }
}

module.exports = login;